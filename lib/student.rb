require 'course'

class Student

  def initialize(first_name,last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_accessor :first_name,:last_name,:courses

  def name
    first_name+ " " +last_name
  end

  def enroll(course)
    self.courses.each do |c|
      if course.conflicts_with?(c)
        raise Exception.new("Conflicting course time")
      end
    end
    unless self.courses.include?(course)
      self.courses.push(course)
      course.students.push(self)
    end
  end

  def course_load
    department_credits=Hash.new(0)
    self.courses.each do |course|
      department_credits[course.department] += course.credits
    end
    department_credits
  end
end
